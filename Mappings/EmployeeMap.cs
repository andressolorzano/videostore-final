﻿using FluentNHibernate.Mapping;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mappings
{
    public class EmployeeMap : ClassMap<Employee>
    {
        public EmployeeMap()
        {
            Table("Employee");
            Id(m => m.Id);
            Map(m => m.DateHired);
            Map(m => m.DateOfBirth);
            //Map(m => m.IsManager);
            Map(m => m.Password);
            Map(m => m.Username);
            Component(c => c.Name, m => {
                m.Map(n => n.Title);
                m.Map(n => n.First);
                m.Map(n => n.Middle);
                m.Map(n => n.Last);
                m.Map(n => n.Suffix);
            });
        }
    }
}