﻿using FluentNHibernate.Mapping;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mappings
{
    public class CommunicationMethodMap: ClassMap<CommunicationMethod>
    {
        public CommunicationMethodMap()
        {
            Table("CommunicationMethod");
            Id(c => c.Id);
            Map(c => c.Name);
            Map(c => c.Frequency);
            Map(c => c.Units);
        }
    }
}
