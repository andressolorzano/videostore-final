﻿using FluentNHibernate.Mapping;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mappings
{
    public class RatingMap: ClassMap<Rating>
    {
        public RatingMap()
        {
            Table("Rating");
            Id(c => c.Id);
            Map(c => c.Comment);
            Map(c => c.Score);
        }
    }
}
