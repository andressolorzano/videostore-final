﻿using FluentNHibernate.Mapping;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mappings
{
    public class RentalMap : ClassMap<Rental>
    {
        public RentalMap()
        {
            Table("Rental");
            Id(m => m.Id);
            Map(m => m.DueDate);
            Map(m => m.RentalDate);
            Map(m => m.ReturnDate);
        }
    }
}