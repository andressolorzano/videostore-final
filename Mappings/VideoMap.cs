﻿using FluentNHibernate.Mapping;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mappings
{
    public class VideoMap : ClassMap<Video>
    {
        public VideoMap ()
        {
            Id(v => v.Id);
            Map(v => v.PurchaseDate);
            Map(v => v.NewArrival);
            References<Store>(v => v.Store).Cascade.All();
            References<Movie>(v => v.Movie, "Movie_Id");
        }
    }
}

