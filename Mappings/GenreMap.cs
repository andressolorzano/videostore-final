﻿using FluentNHibernate.Mapping;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mappings
{
    public class GenreMap : ClassMap<Genre>
    {
        public GenreMap()
        {
            Table("Genres");
            Schema("imdb");
            Id(m => m.Name, "Genre").GeneratedBy.Assigned();
        }
    }
}
