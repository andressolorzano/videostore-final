﻿using FluentNHibernate.Mapping;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mappings
{
    public class CustomerMap : ClassMap<Customer>
    {
        public CustomerMap()
        {
            Id(c => c.Id);
            Map(c => c.EmailAddress);
            Map(c => c.StreetAddress);
            Map(c => c.Phone);
            Map(c => c.Password);
            Component(c => c.Name, m => {
                m.Map(n => n.Title);
                m.Map(n => n.First);
                m.Map(n => n.Middle);
                m.Map(n => n.Last);
                m.Map(n => n.Suffix);
            });
            References<ZipCode>(c => c.ZipCode, "ZipCode").Cascade.All();
            HasManyToMany<Store>(c => c.PreferredStores)
                .Table("Prefers")
                .AsList(index => index.Column("PrefersOrder"))
                .Cascade.All()
                ;                
        }
    }
}
