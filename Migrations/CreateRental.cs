﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(27)]      //change this
    public class CreateRental : Migration
    {
        public override void Down()
        {
            Delete.Table("Rental").InSchema("videostore");
        }

        public override void Up()
        {
            //Add a primary key?
            Create.Table("Rental").InSchema("videostore")
                .WithColumn("Id").AsInt32().Identity().PrimaryKey()
                .WithColumn("DueDate").AsDateTime().Unique().NotNullable()
                .WithColumn("RentalDate").AsDateTime().Unique().NotNullable()
                .WithColumn("ReturnDate").AsDateTime().Unique().NotNullable();
        }
    }
}
