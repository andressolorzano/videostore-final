﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(28)]      //change this
    public class CreateReservation : Migration
    {
        public override void Down()
        {
            Delete.Table("Reservation").InSchema("videostore");
        }

        public override void Up()
        {
            //Add a primary key?
            Create.Table("Reservation").InSchema("videostore")
                .WithColumn("Id").AsInt32().Identity().PrimaryKey()
                .WithColumn("ReservationDate").AsDateTime().Unique().NotNullable();
        }
    }
}