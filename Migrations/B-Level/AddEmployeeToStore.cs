﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace Migrations.B_Level
{
    class AddEmployeeToStore : Migration
    {
        public override void Down()
        {
            Delete.ForeignKey("EmployeeToStore")
                .OnTable("Employee")
                .InSchema("videostore");
        }

        public override void Up()
        {
            Create.ForeignKey("EmployeeToStore")
                .FromTable("Employee")
                .InSchema("videostore")
                .ForeignColumn("Store_Id")
                .ToTable("Store")
                .InSchema("videostore")
                .PrimaryColumn("Id")
                .OnUpdate(System.Data.Rule.Cascade);
        }
    }
}
