﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace Migrations.B_Level
{
    [Migration(32)]
    public class AddRentalToVideo : Migration
    {
        public override void Down()
        {
            Delete.Column("Movie_Id")
                .FromTable("Video")
                .InSchema("videostore");
        }

        public override void Up()
        {
            Create.ForeignKey("VideoToRental")
                           .FromTable("Video")
                           .InSchema("videostore")
                           .ForeignColumn("Rental_Id")
                           .ToTable("Rental")
                           .InSchema("videostore")
                           .PrimaryColumn("Id")
                           .OnDeleteOrUpdate(System.Data.Rule.Cascade);
        }
    }
}
