﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace Migrations.B_Level
{
    public class AddStoreToEmployee : Migration
    {
        public override void Down()
        {
            Delete.Column("Store_Id")
                .FromTable("Employee")
                .InSchema("videostore");
        }

        public override void Up()
        {
            Alter.Table("Employee")
                .InSchema("videostore")
                .AddColumn("Store_Id").AsInt32().NotNullable()
        }
    }
}
