﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace Migrations.B_Level
{
    //[Migration(30)]
    public class AddCustomerToRental : Migration
    {
        public override void Down()
        {
            Delete.Column("Customer_Id")
                 .FromTable("Rental")
                 .InSchema("videostore");
        }

        public override void Up()
        {
            Alter.Table("Rental")
                .InSchema("videostore")
                .AddColumn("Customer_Id").AsInt32();
        }
    }
}
