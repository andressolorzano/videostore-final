﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace Migrations.B_Level
{
    [Migration(33)]
    class AddVideoToRental : Migration
    {
        public override void Down()
        {
            Delete.Column("Rental_Id")
                .FromTable("Video")
                .InSchema("videostore");
        }

        public override void Up()
        {
            Alter.Table("Video")
                .InSchema("videostore")
                .AddColumn("Rental_Id").AsInt32();
        }
    }
}
