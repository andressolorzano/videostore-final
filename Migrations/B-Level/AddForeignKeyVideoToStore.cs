﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(12)]
    public class AddForeignKeyVideoToStore : Migration
    {
        public override void Down()
        {
            Delete.ForeignKey("VideoToStore")
                .OnTable("Video")
                .InSchema("videostore");
        }

        public override void Up()
        {
            Create.ForeignKey("VideoToStore")
                .FromTable("Video")
                .InSchema("videostore")
                .ForeignColumn("Store_Id")
                .ToTable("Store")
                .InSchema("videostore")
                .PrimaryColumn("Id")
                .OnUpdate(System.Data.Rule.Cascade);
        }
    }
}
