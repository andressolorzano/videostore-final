﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(7)]
    public class AddZipCodeToCustomer : Migration
    {
        public override void Down()
        {
            Delete.Column("ZipCode")
                .FromTable("Customer")
                .InSchema("videostore");
        }

        public override void Up()
        {
            Alter.Table("Customer")
                .InSchema("videostore")
                .AddColumn("ZipCode").AsString().NotNullable();
        }
    }
}
