﻿using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace Migrations.B_Level
{
    //[Migration(31)]
    public class AddRentalsToCustomer : Migration
    {
        public override void Down()
        {
            /**
            Delete.Column("Rental_Id")
                .FromTable("Customer")
                .InSchema("videostore");
            **/

            Delete.ForeignKey("RentalsToCustomer")
               .OnTable("Customer")
               .InSchema("videostore");

        }

        public override void Up()
        {
            /**
            Alter.Table("Customer")
                .InSchema("videostore")
                .AddColumn("Rentals").AsString();
            **/
            Create.ForeignKey("RentalsToCustomer")
               .FromTable("Rental")
               .InSchema("videostore")
               .ForeignColumn("Customer_Id")
               .ToTable("Customer")
               .InSchema("videostore")
               .PrimaryColumn("Id")
               .OnUpdate(System.Data.Rule.Cascade);
        }
    }
}
