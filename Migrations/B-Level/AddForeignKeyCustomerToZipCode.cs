﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations.B_Level
{
    [Migration(8)]
    public class AddForeignKeyCustomerToZipCode : Migration
    {
        public override void Down()
        {
            Delete.ForeignKey("CustomerToZipCode")
                .OnTable("Customer")
                .InSchema("videostore");
        }

        public override void Up()
        {
            Create.ForeignKey("CustomerToZipCode")
                .FromTable("Customer")
                .InSchema("videostore")
                .ForeignColumn("ZipCode")
                .ToTable("ZipCode")
                .InSchema("videostore")
                .PrimaryColumn("Code")
                .OnUpdate(System.Data.Rule.Cascade);
        }
    }
}
