﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(26)]      //change this
    public class CreateRating : Migration
    {
        public override void Down()
        {
            Delete.Table("Rating").InSchema("videostore");
        }

        public override void Up()
        {
            //Add a primary key?
            Create.Table("Rating").InSchema("videostore")
                .WithColumn("Id").AsInt32().Identity().PrimaryKey()
                .WithColumn("Comment").AsString(255).Unique().NotNullable() 
                .WithColumn("Score").AsInt32().Unique().NotNullable();
        }
    }
}
