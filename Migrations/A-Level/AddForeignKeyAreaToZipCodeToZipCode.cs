﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(17)]
    public class AddForeignKeyAreaToZipCodeToZipCode : Migration
    {
        public override void Down()
        {
            Delete.ForeignKey("ZipCodeToAreaToZipCode")
                .OnTable("ZipCodeToArea")
                .InSchema("videostore");
        }

        public override void Up()
        {
            Create.ForeignKey("ZipCodeToAreaToZipCode")
                .FromTable("ZipCodeToArea")
                .InSchema("videostore")
                .ForeignColumn("ZipCode_Id")
                .ToTable("ZipCode")
                .InSchema("videostore")
                .PrimaryColumn("Code")
                .OnDeleteOrUpdate(System.Data.Rule.Cascade);
        }
    }
}
