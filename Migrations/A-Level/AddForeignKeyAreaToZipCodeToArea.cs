﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(16)]
    public class AddForeignKeyAreaToZipCodeToArea : Migration
    {
        public override void Down()
        {
            Delete.ForeignKey("ZipCodeToAreaToArea")
                .OnTable("ZipCodeToArea")
                .InSchema("videostore");
        }

        public override void Up()
        {
            Create.ForeignKey("ZipCodeToAreaToArea")
               .FromTable("ZipCodeToArea")
               .InSchema("videostore")
               .ForeignColumn("Area_Id")
               .ToTable("Area")
               .InSchema("videostore")
               .PrimaryColumn("Id")
               .OnDeleteOrUpdate(System.Data.Rule.Cascade);
        }
    }
}
