﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(25)]      //change this
    public class CreateEmployee : Migration
    {
        public override void Down()
        {
            Delete.Table("Employee").InSchema("videostore");
        }

        public override void Up()
        {
            //Add a primary key?
            Create.Table("Employee").InSchema("videostore")
                .WithColumn("Id").AsInt32().Identity().PrimaryKey()
                .WithColumn("DateHired").AsDateTime().Unique().NotNullable()
                .WithColumn("DateOfBirth").AsDateTime().Unique().NotNullable()
                //.WithColumn("IsManager").AsBoolean()
                .WithColumn("Password").AsString(255).NotNullable()
                .WithColumn("Username").AsString(255).NotNullable()
                .WithColumn("Title").AsString().Nullable()
                .WithColumn("First").AsString().NotNullable()
                .WithColumn("Middle").AsString().Nullable()
                .WithColumn("Last").AsString().NotNullable()
                .WithColumn("Suffix").AsString().Nullable();
        }
    }
}
