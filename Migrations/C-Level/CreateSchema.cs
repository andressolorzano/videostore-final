﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(1)]
    public class Schema : Migration
    {
        public override void Down()
        {
            Delete.Schema("videostore");
        }

        public override void Up()
        {
            Create.Schema("videostore");
        }
    }
}
