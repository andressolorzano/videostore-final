﻿using NHibernate;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;
using Mappings;
using FluentNHibernate.Testing;
using VideoStore.Utilities;
using System.Collections;

namespace MappingTests
{
    public class MappingTests
    {
        private ISession _session;
        private Movie starWars;
        private Movie hoosiers;
                
        [SetUp]
        public void CreateSession ()
        {
            _session = SessionFactory.CreateSessionFactory().GetCurrentSession();
            _session.CreateSQLQuery("delete from videostore.Area").ExecuteUpdate();
            _session.CreateSQLQuery("delete from videostore.Customer").ExecuteUpdate();
            _session.CreateSQLQuery("delete from videostore.Video").ExecuteUpdate();
            _session.CreateSQLQuery("delete from videostore.Store").ExecuteUpdate();
            _session.CreateSQLQuery("delete from videostore.ZipCode").ExecuteUpdate();
            _session.CreateSQLQuery("delete from imdb.Genres where Genre='Rock'").ExecuteUpdate();

            starWars = _session.Load<Movie>("tt0076759 ");
            var title = starWars.Title;

            hoosiers = _session.Load<Movie>("tt0091217 ");
            title = hoosiers.Title;
        }

        [Test]
        public void ZipCodeMappingIsCorrect()
        {
            new PersistenceSpecification<ZipCode>(_session)
                .CheckProperty(z => z.Code, "49464")
                .CheckProperty(z => z.City, "Zeeland")
                .CheckProperty(z => z.State, "Michigan");
        }

        [Test]
        public void AreaMappingIsCorrect()
        {
            var zipCodes = new List<ZipCode>
            {
                new ZipCode {
                    Code = "49423",
                    City = "Holland",
                    State = "Michigan"
                },
                new ZipCode
                {
                    Code = "48444",
                    City = "Imlay City",
                    State = "Michigan"
                }
            };

            new PersistenceSpecification<Area>(_session, new DateEqualityComparer())
                .CheckProperty(a => a.Name, "Zeeland")                
                .CheckBag(a => a.ZipCodes, zipCodes)
                .VerifyTheMappings();
        }
                       
        [Test]
        public void CustomerMappingIsCorrect()
        {
            var zeeland = new ZipCode
            {
                Code = "49464",
                City = "Zeeland",
                State = "Michigan"
            };

            var stores = new List<Store>
            {
                new Store
                {
                    StoreName = "Store 2",
                    StreetAddress = "1234 Winterwood Lane",
                    PhoneNumber = "616-748-9715",
                    ZipCode = zeeland
                },
                new Store
                {
                    StoreName = "Store 1",
                    StreetAddress = "1260 Winterwood Lane",
                    PhoneNumber = "616-123-4567",
                    ZipCode = zeeland
                },
            };

            new PersistenceSpecification<Customer>(_session)
                .CheckProperty(c => c.Name, new Name()
                {
                    Title = "Dr.",
                    First = "Ryan",
                    Middle = "L",
                    Last = "McFall",
                    Suffix = "Sr"
                })
                .CheckProperty(c => c.EmailAddress, "mcfall@hope.edu")
                .CheckProperty(c => c.StreetAddress, "27 Graves Place VWF 220")
                .CheckProperty(c => c.Password, "Abc123$!")
                .CheckProperty(c => c.Phone, "616-395-7952")
                .CheckReference(c => c.ZipCode,
                    new ZipCode()
                    {
                        Code = "49423",
                        City = "Holland",
                        State = "Michigan"
                    }
                )   
                .CheckInverseList(
                    c => c.PreferredStores, stores, 
                    (cust, store) => cust.AddPreferredStore(store)
                )
                .VerifyTheMappings();
        }

        [Test]
        public void StoreMappingIsCorrect()
        {
            var videos = new List<Video>()
            {
                new Video
                {
                    Movie = starWars,
                    NewArrival = false,
                    PurchaseDate = DateTime.Now
                },
                new Video
                {
                    Movie = hoosiers,
                    NewArrival = true, 
                    PurchaseDate = DateTime.Now.AddDays(1)
                }
            };

            new PersistenceSpecification<Store>(_session)
                .CheckProperty(s => s.StoreName, "Blockbusted")
                .CheckProperty(s => s.StreetAddress, "1234 Broke Street")
                .CheckProperty(s => s.PhoneNumber, "616-666-0000")
                .CheckReference(s => s.ZipCode, 
                    new ZipCode() {
                        Code = "49423",
                        City = "Holland",
                        State = "Michigan"
                    }
                )
                .CheckInverseList( s => s.Videos, videos, (s, v) => s.AddVideo(v))
                .VerifyTheMappings();
        }

        [Test]
        public void VideoMappingIsCorrect()
        {
            new PersistenceSpecification<Video>(_session, new DateEqualityComparer())
                .CheckProperty(v => v.NewArrival, true)
                .CheckProperty(v => v.PurchaseDate, DateTime.Now)
                .CheckReference(v => v.Store, 
                    new Store()
                    {
                        StoreName = "Last Store",
                        StreetAddress = "1234 Broke St",
                        PhoneNumber = "616-666-0000",
                        ZipCode = new ZipCode()
                        {
                            Code = "49423",
                            City = "Holland",
                            State = "Michigan"
                        }
                    }
                )
                .CheckReference(v => v.Movie, starWars)
                .VerifyTheMappings();
        }

        [Test]
        public void MovieMappingIsCorrect()
        {            
            Assert.AreEqual("tt0076759 ", starWars.TitleId);
            Assert.AreEqual("Star Wars: Episode IV - A New Hope", starWars.Title);
            Assert.AreEqual("Star Wars", starWars.OriginalTitle);
            Assert.AreEqual(1977, starWars.Year);
            Assert.AreEqual(121, starWars.RunningTimeInMinutes);
            Assert.AreEqual("PG", starWars.Rating);
            Assert.AreEqual("Action", starWars.PrimaryGenre.Name);
            //Console.WriteLine("Should be 3: " + starWars.Genres.Count);
            Assert.AreEqual(4, starWars.Genres.Count);
            Assert.AreEqual("Action", starWars.Genres[1].Name);
            Assert.AreEqual("Adventure", starWars.Genres[2].Name);
            Assert.AreEqual("Fantasy", starWars.Genres[3].Name);
            /**
            var genres = new List<Genre>()
            {
                new Genre()
                {
                    Name = "Action"
                },
                new Genre()
                {
                    Name = "Romance"
                }
            };
            
            new PersistenceSpecification<Movie>(_session)
                //Come back here!
                //.CheckReference(v => v.PrimaryGenre, new Genre() { Name = "Comedy" })
                .CheckInverseList(s => s.Genres, genres, (s, v) => s.Genres.Add(v))
                .VerifyTheMappings();
           **/

        }

        [Test]
        public void GenreMappingIsCorrect()
        {
            new PersistenceSpecification<Genre>(_session)
                .CheckProperty(z => z.Name, "Rock")
                .VerifyTheMappings();
        }

        [Test]
        public void CommunicationMethodMappingIsCorrect()
        {
            new PersistenceSpecification<CommunicationMethod>(_session, new DateEqualityComparer())
                .CheckProperty(c => c.Name, "Rock")
                .CheckProperty(c => c.Frequency, 123)
                .CheckProperty(c => c.Units, CommunicationMethod.TimeUnit.Day)
                .VerifyTheMappings();
        }

        [Test]
        public void RatingIsCorrect()
        {
            new PersistenceSpecification<Rating>(_session)
                .CheckProperty(c => c.Comment, "Rock")
                .CheckProperty(c => c.Score, 2)
                .VerifyTheMappings();
        }

        [Test]
        public void EmployeeIsCorrect()
        {
            new PersistenceSpecification<Employee>(_session, new DateEqualityComparer())
                .CheckProperty(c => c.DateHired, DateTime.Now)
                .CheckProperty(c => c.DateOfBirth, DateTime.Now.AddYears(-17))
                //.CheckProperty(c => c.IsManager, true)
                .CheckProperty(c => c.Password, "L2ksjjdjd")
                .CheckProperty(c => c.Username, "blahblahablahah")
                .CheckProperty(c => c.Name, new Name() 
                    { 
                        First ="Isabella", 
                        Middle = "L",
                        Last = "Lemus"
                    }
                )
                .VerifyTheMappings();
        }

        [Test]
        public void RentalIsCorrect()
        {
            new PersistenceSpecification<Rental>(_session, new DateEqualityComparer())
                .CheckProperty(c => c.DueDate, DateTime.Now)
                .CheckProperty(c => c.RentalDate, DateTime.Now)
                .CheckProperty(c => c.ReturnDate, DateTime.Now);
        }

        [Test]
        public void ReservationIsCorrect()
        {
            new PersistenceSpecification<Reservation>(_session, new DateEqualityComparer())
                .CheckProperty(c => c.ReservationDate, DateTime.Now)
                .VerifyTheMappings();
        }

        [Test]
        public void RentalsInCustomerIsCorrect()
        {
            var cust = new Customer()
            {
                Name = new Name()
                {
                    First = "Dre",
                    Middle = "L",
                    Last = "Cilantro",
                    Suffix = "III",
                    Title = "Dr."
                },
                EmailAddress = "andres.solorzano@hope.edu",
                Password = "Passw0rd",
                Phone = "911",
                StreetAddress = "12345 GingerBread Lane",
                ZipCode = new ZipCode()
                {
                    Code = "49423",
                    City = "Holland",
                    State = "Michigan"
                }
            };
            var cust_Id = cust.Id;

            var vid = new Video()
            {
                Movie = starWars,
                PurchaseDate = DateTime.Now,
                NewArrival = false
            };

            var vid2 = new Video()
            {
                Movie = hoosiers,
                PurchaseDate = DateTime.Now,
                NewArrival = true
            };

            var rent1 = new Rental(vid, cust)
            {
                DueDate = DateTime.Now.AddDays(3),
                RentalDate = DateTime.Now.AddDays(1),
                ReturnDate = DateTime.Now.AddDays(2)
            };

            var rent2 = new Rental(vid2, cust)
            {
                DueDate = DateTime.Now.AddDays(6),
                RentalDate = DateTime.Now.AddDays(2),
                ReturnDate = DateTime.Now.AddDays(4)
            };

            var rentals = new List<Rental>()
            {
                rent1,
                rent2
            };
            _session.Save(cust);
            _session.Save(rent1);
            _session.Save(rent2);
            _session.Evict(cust);
            _session.Get<Customer>(cust_Id);
            Assert.AreEqual("Rentals", rentals);
        }
    }
}
